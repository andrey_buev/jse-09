package ru.buevas.tm.service;

import java.util.List;
import ru.buevas.tm.entity.Project;
import ru.buevas.tm.repository.ProjectRepository;

/**
 * Сервис проектов
 *
 * Делегирует операции над проектами репозиторию с предшествующей обработкой и валидацией поступающей информации
 *
 * @author Andrey Buev
 * @see ProjectRepository
 */
public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        return projectRepository.create(name);
    }

    public Project create(String name, String description) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (description == null) {
            return null;
        }
        return projectRepository.create(name, description);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project update(Long id, String name, String description) {
        if (id == null) {
            return null;
        }
        if (name == null || name.isEmpty() || description == null) {
            return null;
        }
        return projectRepository.update(id, name, description);
    }

    public Project findByIndex(int index) {
        return projectRepository.findByIndex(index);
    }

    public Project findById(Long id) {
        return projectRepository.findById(id);
    }

    public Project findByName(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        return projectRepository.findByName(name);
    }

    public Project removeByIndex(int index) {
        return projectRepository.removeByIndex(index);
    }

    public Project removeById(Long id) {
        return projectRepository.removeById(id);
    }

    public Project removeByName(String name) {
        return projectRepository.removeByName(name);
    }

    public void clear() {
        projectRepository.clear();
    }
}
