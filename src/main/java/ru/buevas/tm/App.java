package ru.buevas.tm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import ru.buevas.tm.constant.TerminalConst.CliCommands;
import ru.buevas.tm.controller.ProjectController;
import ru.buevas.tm.controller.SystemController;
import ru.buevas.tm.controller.TaskController;
import ru.buevas.tm.repository.ProjectRepository;
import ru.buevas.tm.repository.TaskRepository;
import ru.buevas.tm.service.ProjectService;
import ru.buevas.tm.service.TaskService;

/**
 * Основной класс приложения
 *
 * @author Andrey Buev
 */
public class App {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskController taskController = new TaskController(taskService);

    private final SystemController systemController = new SystemController();

    /**
     * Точка входа в приложение
     *
     * @param args - параметры командной строки, переданные при старте приложения
     */
    public static void main(String[] args) {
        final App app = new App();
        app.systemController.printWelcome();
        app.run(args);
        app.process();
    }

    /**
     * Запуск приложения с аргументом из командной строки
     *
     * @param args - массив аргументов
     */
    public void run(final String[] args) {
        if (args == null || args.length < 1) {
            return;
        }

        final String command = args[0];
        System.exit(execCommand(command));
    }

    /**
     * Запуск приложения в режиме бесконечного цикла
     */
    public void process() {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String command = "";
        while (!CliCommands.EXIT.equals(command)) {
            try {
                command = reader.readLine();
                execCommand(command);
                System.out.println();
            } catch (IOException e) {
                systemController.printError(e.getMessage());
            }
        }
    }

    /**
     * Выполнение команды
     *
     * @param command - выполняемая команда
     * @return код ошибки или 0 в случае успешного завершения
     */
    public int execCommand(final String command) {
        if (command == null || command.isEmpty()) {
            return -1;
        }
        switch (command) {
            case CliCommands.VERSION: {
                return systemController.printVersion();
            }
            case CliCommands.ABOUT: {
                return systemController.printAbout();
            }
            case CliCommands.HELP: {
                return systemController.printHelp();
            }
            case CliCommands.EXIT: {
                return systemController.exit();
            }
            case CliCommands.PROJECT_CREATE: {
                return projectController.createProject();
            }
            case CliCommands.PROJECT_LIST: {
                return projectController.listProject();
            }
            case CliCommands.PROJECT_UPDATE_BY_INDEX: {
                return projectController.updateProjectByIndex();
            }
            case CliCommands.PROJECT_UPDATE_BY_ID: {
                return projectController.updateProjectById();
            }
            case CliCommands.PROJECT_VIEW_BY_INDEX: {
                return projectController.viewProjectByIndex();
            }
            case CliCommands.PROJECT_VIEW_BY_ID: {
                return projectController.viewProjectById();
            }
            case CliCommands.PROJECT_VIEW_BY_NAME: {
                return projectController.viewProjectByName();
            }
            case CliCommands.PROJECT_REMOVE_BY_INDEX: {
                return projectController.removeProjectByIndex();
            }
            case CliCommands.PROJECT_REMOVE_BY_ID: {
                return projectController.removeProjectById();
            }
            case CliCommands.PROJECT_REMOVE_BY_NAME: {
                return projectController.removeProjectByName();
            }
            case CliCommands.PROJECT_CLEAR: {
                return projectController.clearProject();
            }
            case CliCommands.TASK_CREATE: {
                return taskController.createTask();
            }
            case CliCommands.TASK_LIST: {
                return taskController.listTask();
            }
            case CliCommands.TASK_UPDATE_BY_INDEX: {
                return taskController.updateTaskByIndex();
            }
            case CliCommands.TASK_UPDATE_BY_ID: {
                return taskController.updateTaskById();
            }
            case CliCommands.TASK_VIEW_BY_INDEX: {
                return taskController.viewTaskByIndex();
            }
            case CliCommands.TASK_VIEW_BY_ID: {
                return taskController.viewTaskById();
            }
            case CliCommands.TASK_VIEW_BY_NAME: {
                return taskController.viewTaskByName();
            }
            case CliCommands.TASK_REMOVE_BY_INDEX: {
                return taskController.removeTaskByIndex();
            }
            case CliCommands.TASK_REMOVE_BY_ID: {
                return taskController.removeTaskById();
            }
            case CliCommands.TASK_REMOVE_BY_NAME: {
                return taskController.removeTaskByName();
            }
            case CliCommands.TASK_CLEAR: {
                return taskController.clearTask();
            }
            default: {
                return systemController.printError("Unknown parameter");
            }
        }
    }
}
