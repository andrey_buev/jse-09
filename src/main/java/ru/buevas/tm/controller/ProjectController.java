package ru.buevas.tm.controller;

import java.io.IOException;
import java.util.List;
import ru.buevas.tm.entity.Project;
import ru.buevas.tm.service.ProjectService;

/**
 * Контроллер для операций над проектами
 *
 * @author Andrey Buev
 */
public class ProjectController extends BaseController {

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    /**
     * Обработка команды создания проекта
     */
    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        try {
            System.out.print("Enter project name: ");
            final String name = reader.readLine();
            System.out.print("Enter project description: ");
            final String description = reader.readLine();
            projectService.create(name, description);
            System.out.println("[SUCCESS]");
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды вывода списка проектов
     */
    public int listProject() {
        System.out.println("[LIST PROJECT]");
        System.out.println("Available projects:");
        List<Project> projects = projectService.findAll();
        if (projects.isEmpty()) {
            System.out.println("Empty");
        } else {
            int index = 1;
            for (Project project : projects) {
                System.out.printf("%d. %s - %s%n", index, project.getId(), project.getName());
                index++;
            }
        }
        System.out.println("[SUCCESS]");
        return 0;
    }

    /**
     * Обработка команды обновления проекта по номеру в списке
     */
    public int updateProjectByIndex() {
        System.out.println("[Update PROJECT]");
        try {
            System.out.print("Enter project index: ");
            int index = Integer.parseInt(reader.readLine()) - 1;
            Project project = projectService.findByIndex(index);
            if (project == null) {
                printError("Project not found");
                return 0;
            }
            System.out.print("Enter project name: ");
            final String name = reader.readLine();
            System.out.print("Enter project description: ");
            final String description = reader.readLine();
            projectService.update(project.getId(), name, description);
            System.out.println("[SUCCESS]");
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды обновления проекта по идентификатору
     */
    public int updateProjectById() {
        System.out.println("[Update PROJECT]");
        try {
            System.out.print("Enter project id: ");
            Long id = Long.parseLong(reader.readLine());
            Project project = projectService.findById(id);
            if (project == null) {
                printError("Project not found");
                return 0;
            }
            System.out.print("Enter project name: ");
            final String name = reader.readLine();
            System.out.print("Enter project description: ");
            final String description = reader.readLine();
            projectService.update(project.getId(), name, description);
            System.out.println("[SUCCESS]");
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Вывод на экран информации о проекте
     *
     * @param project - проект
     * @see Project
     */
    private void printProject(final Project project) {
        if (project == null) {
            return;
        }
        System.out.println("[VIEW PROJECT]");
        System.out.println("id: " + project.getId());
        System.out.println("name: " + project.getName());
        System.out.println("description: " + project.getDescription());
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды отображения проекта по номеру в списке
     */
    public int viewProjectByIndex() {
        try {
            System.out.print("Enter index: ");
            int index = Integer.parseInt(reader.readLine()) - 1;
            Project project = projectService.findByIndex(index);
            if (project != null) {
                printProject(project);
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды отображения проекта по идентификатору
     */
    public int viewProjectById() {
        try {
            System.out.print("Enter id: ");
            Long id = Long.parseLong(reader.readLine());
            Project project = projectService.findById(id);
            if (project != null) {
                printProject(project);
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды отображения проекта по имени
     */
    public int viewProjectByName() {
        try {
            System.out.print("Enter name: ");
            String name = reader.readLine();
            Project project = projectService.findByName(name);
            if (project != null) {
                printProject(project);
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды удаления проекта по номеру в списке
     */
    public int removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        try {
            System.out.print("Enter index: ");
            int index = Integer.parseInt(reader.readLine()) - 1;
            Project project = projectService.removeByIndex(index);
            if (project != null) {
                System.out.println("[SUCCESS]");
            } else {
                System.out.println("[ERROR]");
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды удаления проекта по идентификатору
     */
    public int removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        try {
            System.out.print("Enter id: ");
            Long id = Long.parseLong(reader.readLine());
            Project project = projectService.removeById(id);
            if (project != null) {
                System.out.println("[SUCCESS]");
            } else {
                System.out.println("[ERROR]");
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды удаления проекта по имени
     */
    public int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        try {
            System.out.print("Enter name: ");
            String name = reader.readLine();
            Project project = projectService.removeByName(name);
            if (project != null) {
                System.out.println("[SUCCESS]");
            } else {
                System.out.println("[ERROR]");
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды отчистки списка проектов
     */
    public int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[SUCCESS]");
        return 0;
    }
}
