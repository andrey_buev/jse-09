package ru.buevas.tm.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Базовый контроллер
 *
 * @author Andrey Buev
 */
public class BaseController {

    protected final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    /**
     * Вывод на экран сообщения об ошибке
     *
     * @param message - сообщение
     * @return -1
     */
    public int printError(final String message) {
        System.out.println("[ERROR]");
        System.out.println(message);
        return -1;
    }
}
