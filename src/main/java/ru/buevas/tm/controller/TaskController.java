package ru.buevas.tm.controller;

import java.io.IOException;
import java.util.List;
import ru.buevas.tm.entity.Task;
import ru.buevas.tm.service.TaskService;

/**
 * Контроллер для операций над задачами
 *
 * @author Andrey Buev
 */
public class TaskController extends BaseController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    /**
     * Обработка команды создания задачи
     */
    public int createTask() {
        System.out.println("[CREATE TASK]");
        try {
            System.out.print("Enter task name: ");
            final String name = reader.readLine();
            System.out.print("Enter task description: ");
            final String description = reader.readLine();
            taskService.create(name, description);
            System.out.println("[SUCCESS]");
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды вывода списка задач
     */
    public int listTask() {
        System.out.println("[LIST TASK]");
        System.out.println("Available tasks:");
        List<Task> tasks = taskService.findAll();
        if (tasks.isEmpty()) {
            System.out.println("Empty");
        } else {
            int index = 1;
            for (Task task : tasks) {
                System.out.printf("%d. %s - %s%n", index, task.getId(), task.getName());
                index++;
            }
        }
        System.out.println("[SUCCESS]");
        return 0;
    }

    /**
     * Обработка команды обновления задачи по номеру в списке
     */
    public int updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        try {
            System.out.print("Enter task index: ");
            int index = Integer.parseInt(reader.readLine()) - 1;
            Task task = taskService.findByIndex(index);
            if (task == null) {
                printError("Task not found");
                return 0;
            }
            System.out.print("Enter task name: ");
            final String name = reader.readLine();
            System.out.print("Enter task description: ");
            final String description = reader.readLine();
            taskService.update(task.getId(), name, description);
            System.out.println("[SUCCESS]");
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды обновления задачи по номеру в списке
     */
    public int updateTaskById() {
        System.out.println("[UPDATE TASK]");
        try {
            System.out.print("Enter task id: ");
            Long id = Long.parseLong(reader.readLine());
            Task task = taskService.findById(id);
            if (task == null) {
                printError("Task not found");
                return 0;
            }
            System.out.print("Enter task name: ");
            final String name = reader.readLine();
            System.out.print("Enter task description: ");
            final String description = reader.readLine();
            taskService.update(task.getId(), name, description);
            System.out.println("[SUCCESS]");
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Вывод на экран информации о задаче
     *
     * @param task - задача
     * @see Task
     */
    private void printTask(final Task task) {
        if (task == null) {
            return;
        }
        System.out.println("[VIEW TASK]");
        System.out.println("id: " + task.getId());
        System.out.println("name: " + task.getName());
        System.out.println("description: " + task.getDescription());
        System.out.println("[SUCCESS]");
    }

    /**
     * Обработка команды отображения задачи по номеру в списке
     */
    public int viewTaskByIndex() {
        try {
            System.out.print("Enter index: ");
            int index = Integer.parseInt(reader.readLine()) - 1;
            Task task = taskService.findByIndex(index);
            if (task != null) {
                printTask(task);
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды отображения задачи по ижентификатору
     */
    public int viewTaskById() {
        try {
            System.out.print("Enter id: ");
            Long id = Long.parseLong(reader.readLine());
            Task task = taskService.findById(id);
            if (task != null) {
                printTask(task);
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды отображения задачи по имени
     */
    public int viewTaskByName() {
        try {
            System.out.print("Enter name: ");
            String name = reader.readLine();
            Task task = taskService.findByName(name);
            if (task != null) {
                printTask(task);
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды удаления проекта по номеру в списке
     */
    public int removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        try {
            System.out.print("Enter index: ");
            int index = Integer.parseInt(reader.readLine()) - 1;
            Task task = taskService.removeByIndex(index);
            if (task != null) {
                System.out.println("[SUCCESS]");
            } else {
                System.out.println("[ERROR]");
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды удаления проекта по идентификатору
     */
    public int removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        try {
            System.out.print("Enter id: ");
            Long id = Long.parseLong(reader.readLine());
            Task task = taskService.removeById(id);
            if (task != null) {
                System.out.println("[SUCCESS]");
            } else {
                System.out.println("[ERROR]");
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды удаления проекта по имени
     */
    public int removeTaskByName() {
        System.out.println("[REMOVE TASK BY NAME]");
        try {
            System.out.print("Enter name: ");
            String name = reader.readLine();
            Task task = taskService.removeByName(name);
            if (task != null) {
                System.out.println("[SUCCESS]");
            } else {
                System.out.println("[ERROR]");
            }
        } catch (IOException e) {
            printError(e.getMessage());
        }
        return 0;
    }

    /**
     * Обработка команды отчистки списка задач
     */
    public int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[SUCCESS]");
        return 0;
    }
}
